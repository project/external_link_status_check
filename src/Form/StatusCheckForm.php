<?php
/**
 * @file
 * Contains Drupal\external_link_status_check\Form\StatusCheckForm.
 */

namespace Drupal\external_link_status_check\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class StatusCheckForm.
 *
 * @package Drupal\external_link_status_check\Form
 */
class StatusCheckForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'external_link_status_check.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'statuscheck_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('external_link_status_check.settings');
    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Enter Message to show in popup for valid Link'),
      '#description' => $this->t('Enter Message to show in popup for valid Link'),
      '#default_value' => $config->get('body'),
      '#format' => 'full_html',
    ];
    $form['body1'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Enter Message to show in popup for non valid Link'),
      '#description' => $this->t('Enter Message to show in popup for non valid Link'),
      '#default_value' => $config->get('body1'),
      '#format' => 'full_html',
    ];
  
    return parent::buildForm($form, $form_state);  
    
  }

  /**  
   * {@inheritdoc}  
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {  
    parent::submitForm($form, $form_state);  
    $this->config('external_link_status_check.settings')  
		->set('event_password', $form_state->getValue('event_password'))
    ->set('body', $form_state->getValue('body')['value'])
    ->set('body1', $form_state->getValue('body1')['value'])
		->save();  
  }
}