<?php

namespace Drupal\external_link_status_check\Controller;

use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\ControllerBase;

/**
 * Get External link and check that.
 *
 * @package Drupal\external_link_status_check\Controller
 */
class StatusController extends ControllerBase {

  /**
   * Get URL in post request.
   */
  public function status(Request $request) {
    $link = $request->request->get('hyperlink_value');
   
    $json_string = \Drupal::request()->getContent();
    $decoded = Json::decode($json_string);
    try {
      $client = \Drupal::httpClient();
      $request = $client->request('GET', $decoded['hyperlinkValue']);
      // $request = $client->request('GET', 'www.brexhcp.com/xyz');
      $responseStatus = $request->getStatusCode();
      if ($responseStatus == 200) {
        $status = 1;
      }
    }
    catch (\Exception $e) {
      if ($responseStatus == 500) {
        $status = 0;
      }
      \Drupal::logger('external_link_status_check')->error($e->getMessage());
     return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], 500);
  }
  $return[] = ['status' => $status];
  return new JsonResponse($status);
  }

}


