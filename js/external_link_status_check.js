(function (jQuery, Drupal) {
  'use strict';
  Drupal.behaviors.popupModel = {
    attach(context) {
     jQuery(document, context).ready(function () {
        var jq = jQuery.noConflict();
        jq('a[target="_blank"]', context).click(function (event) { 
          event.preventDefault();
          const hyperlinkValue = jQuery(this).attr('href');
          var content = '<div id="content"></div>';
          
          jQuery('header').html(drupalSettings.external_link_status_check.body1);
          jQuery.ajax({
            type: 'POST',
            url: Drupal.url('status-code'),
            data: JSON.stringify({ hyperlinkValue: hyperlinkValue }),
            contentType: "application/json",
            dataType: 'json',
            success: function (data) {
              if (data) {
               event.preventDefault();
               jQuery('header').html(drupalSettings.external_link_status_check.body);
                jQuery('.center').show();
                jQuery('.external-confirm').off('click').on('click', function() {
                  window.open(hyperlinkValue, '_blank');
                });
                jQuery('.close').click(function (event) { 
                  jQuery('.center').css("display", "none");
                  jQuery('.center').hide();
                });
              } 
            },
            error: function(xhr, status, error) 
            {
              console.log('Error:', error);
              jQuery('header').html(drupalSettings.external_link_status_check.body1);
              jQuery('.center1').show();
              jQuery('.close').click(function (event) { 
                jQuery('.center1').css("display", "none");
                jQuery('.center1').hide();
              });
            }
          });
        });
      });

    },
  };
})(jQuery, Drupal);
